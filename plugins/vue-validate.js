import { extend } from "vee-validate";
import { digits, required, confirmed, email } from "vee-validate/dist/rules";

extend("required", {
  ...required,
  message: "Поле обязательно",
});
extend("digits", {
  ...digits,
  message: "Поле Номер телефона должен быть",
});
extend("confirmed", {
  ...confirmed,
  message: "Пароли не совпадает",
});
extend("email", {
  ...email,
  message: "Пароли не совпадает",
});
