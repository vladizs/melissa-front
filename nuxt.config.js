export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  
  head: {
    title: 'Melissa',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Melissa — современный удобный интернет-магазин медикаментов, в котором можно купить онлайн сертифицированные лекарственные средства, витамины, товары для детей и мам, БАДы, косметику и другие товары для здоровья, красоты и гигиены по низким ценам. На нашем сайте вы найдете свыше 3000 наименований от мировых производителей. Общий ассортимент интернет аптеки включает более 20 000 фармацевтических, косметологических и медицинских изделий. Мы предлагаем низкие цены и выгодные условия, а также гарантируем качество представленных на сайте товаров.' },
      { hid: 'keywords', name: 'keywords', content: 'melissa, Melissa, Аптека, Лекарстава, Здоровье, Алматы' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],

  },

  ssr: false,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/global.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vue-range-component.client.js'},
    { src: '~/plugins/persisted-state.client.js'},
    { src: '~/plugins/vuelidate.client.js' },
    { src: '~/plugins/datepicker.client.js'},
    { src: '~/plugins/ymapPlugin.js',  mode: 'client' },
    { src: '~/plugins/vue-validate.js',  ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/composition-api/module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'cookie-universal-nuxt'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseUrl: 'https://api.melissaapteka.kz/api',
    credentials: false
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
}
